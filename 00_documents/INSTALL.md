# Installation of Smart City PoC Environment

The repository provides a 4 Step Guide to install a preconfigured Smart City PoC Environment.

Steps 1 - 3 only describe the preparation of the configuration.

In Step 4 you will install the environment with these 4 Ansible Playbooks

* Playbook 1: Prepare a Base System
* Playbook 2: Install Docker and needed tools
* Playbook 3: Deploy and configure containers
* Playbook 4: Deploy API Configurations

## Prerequisites

You need at least the following infrastructure to run the environment on a remote host:

* 4 vCPUs
* 8 GB of RAM
* 50 + X GB of SSD (depending on your PoC Data)
* Ubuntu 20.04 LTS or Debian 10 as Operating System
* Login Data for root User (will be deactivated during installation)
* The Server must be reachable from the Internet to make Let's Encrypt Mechanism working
* In best case you use a wildcard (sub-)domain for the Remote Servers IP, e.g. `*.sandbox.<domain.tld>`

  In this case, all hosts names that are needed get an working hostname. All currently used hostnames can be found at the end of this document.
* Important: On the remote machine the remote login for root must be enabled for the first playpook. If this is not active by default, please login to the machine and execute the following steps:

  Edit file `/etc/ssh/sshd_config` and set the value `PermitRootLogin yes`.

  Save and close the file.

  Execute `systemctl restart sshd.service`

Additionally you need a „local“ machine to run Ansible with installed sshpass. This will be referenced in the following as „local system“ but can be any Ansible compatible system you have access to.

## Installation Step 1

### Install Ansible

To use the repository an Ansible Installation on the local system is needed. We recommend to install Ansible via pip. To do this, please follow the official [Ansible Documentation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-with-pip).

In addition, please install `jmespath` that is used to set facts in the playbooks (e.g. `sudo apt install python3-jmespath`).

### Clone the repository

Clone this repository to a location you like on you local system.

## Installation Step 2

### Configure the remote host

In the root folder of the repository a `inventory.default`. Please copy this to a files called `inventory` and adjust the contents to your needs. Normally, you only have to add you IP address of the remote system in this line:

``` bash
#########################################################################
# The "servers" groups contains the remote hosts
#########################################################################

[servers]
demo ansible_host=<ip>
```

### Configure central external values for the deployment

The repository contains several Ansible Playbooks - each in a seperate subfolder. The starting numbers of the the subfolders define the sequence to execute the playbooks.

All Playbooks have seperate Variable Sets defined. These are defined by the vars/default.yaml in the respective subfolder. these variables are used internally for the configuration of the components.

External values are defined in the `inventory` file again. Please configure the following variables according to your needs.

``` bash
####################################################
# External Data - please provide valid configuration
####################################################

# Subdomain for the PoC Installation
DOMAIN=<server.domain.com>
# Emails Address for Let's Encrypt Registration
EMAIL=dns@urban-data.cloud

# Unix User Name to run to Platform
CREATE_USER=mrdocker
# Filename of private ssh key to use - standard: id_rsa_sc_admin
SSH_KEY_NAME='id_rsa_sc_admin'

# Admin Password for installation
SC_ADMIN_PASSWORD='<password>'

# Sender Email Address for generated Emails
SC_EMAIL_FROM='demo@<server.domain.com>'
# Email SMTP Server
SC_EMAIL_SERVER='<email.domain.com>'
# Email Server Login
SC_EMAIL_USER='<user>'
# Email Server Password
SC_EMAIL_PASSWORD='<password>'

# API Key for Open Weather Map Access
SC_OWM_API_KEY='<OWN_API_KEY>'
# ID of wished OWM Station
SC_OWM_STATION='OWM_STATION_ID'
# ID of Wished Open Sense Map Sensor
SC_OSM_SENSOR_ID='<OSM_SENSOR_ID>'

# Git User to clone
NR_GIT_LOGIN='<user>'
# Git Token or Password.
# Depending on your authentication method please provide you login credentials (e.g. if you use 2FA use a specific token and so on)
NR_GIT_TOKEN='<token>'
# Repo for internal data Flows (Broker to Timeseries)
# Example: gitlab.com/urban-data/demo-flows/urban-data-internal-demo-flows.git
NR_INT_GIT_REPO='gitlab.com/urban-data/demo-flows/urban-data-internal-demo-flows.git' 
# Repo for external flows (external API Integration)
# Example: gitlab.com/urban-data/demo-flows/urban-data-external-demo-flows.git
NR_EXT_GIT_REPO='gitlab.com/urban-data/demo-flows/urban-data-external-demo-flows.git' 
# Repo Target folder
NR_REPO_TARGET_FOLDER='sc-node-red-flows'
# Key to encrypt/decrypt Nodered Secrets in Node Red Project
NR_SECRETS_KEY='<nodered_secrets_key>'

# Open Weather Map API Key
SC_OWM_API_KEY='<OWM_API_KEY>'
# Open Weather Map Station ID
SC_OWM_STATION='<OWM_STATION_ID>'
# Open Sensor Map Sensor ID
SC_OSM_SENSOR_ID='<OSM_SENSOR_ID>'

# TimescaleDB Password
TIMESCALE_PASSWORD='<password>'

# FROST-DB Password
FROST_DB_PASSWORD='<password>'

# Key for Session Encryption of keycloak-gatekeyer (16 or 32 signs)
AUTH_ENCRYPTION_KEY='<AUTH_KEY>'
```

## Installation Step 3

To secure the remote host, we configure the remote host to only accept SSH Login with Public/Private Key Authentication. For this, an existing key is needed, or will be created by the playbook. If an existing key shall be used, please store it into your local `~/.ssh` Folder and set the environment variable `SSH_KEY_NAME` to its name.

## Installation Step 4

The follwing steps tun Ansible Playbooks. The commands are prepared to run the Playbooks from the root folder of the repository. If you start from another location, you have to adopt the paths.

### Basic Server Configuration

The first steps configures the base system. You have to provide the root Password for this first step - all others use the new user. To log in with password, you must have installed `sshpass` (e.g. `sudo apt-get install sshpass`).

``` bash
ansible-playbook 01_setup_base/playbook.yml -l demo -u root -k -i inventory
```

If you want to use the playbook without interaction (e.g. in a CI/CD pipeline), you can provide the password on the command line `-e 'ansible_password=YourRootPassword'` or in your inventory file:

``` bash
[all:vars] 
ansible_password=YourRootPassword
```

If you use the inventory, please omit the `-k` parameter for `ansible-playbook`.

### Docker Installation

``` bash
ansible-playbook 02_setup_docker/playbook.yml -l demo -u mrdocker --private-key ~/.ssh/id_rsa_sc_admin -i inventory
```

### Basic Platform Installation

The basic platform installation includes a traefik service that is configured to automatically get letsencrypt certificates for all sub domains. If you want to use an existing certificates store, place the traefik `acme.json` file to `03_setup_containers_plattform/files/trafik`. It will be pushed into the `traefik certs volume` by ansible. The `acme.json` can be obtained after a first complete deployment from the traefik container from the `/certs` directory.

``` bash
ansible-playbook 03_setup_containers_plattform/playbook.yml -l demo -u mrdocker --private-key ~/.ssh/id_rsa_sc_admin -i inventory
```


## Use the PoC Environment

Now you are ready to use the preconfigured PoC Environment. These are the important URLs to use:

* https://idm.<server.domain.com>             --> IDM Keycloak
* https://api.<server.domain.com>/ 			      --> API Portal
* https://apigw.<server.domain.com>/       	  --> API Gateway
* https://api.<server.domain.com>/manage/     --> API Management UI (mind trailing slash)
* https://api.<server.domain.com>/management/ --> API Management API
* https://api.<server.domain.com>/portal/     --> API Portal API
* https://portainer.<server.domain.com>       --> Container Management
* https://nr-admin-int.<server.domain.com>    --> Nodered UI internal Flows
* https://nr-admin-ext.<server.domain.com>    --> Nodered UI external Flows
* https://grafana.<server.domain.com>         --> Grafana for Data Management
* https://monitoring.<server.domain.com>      --> Grafana for System Monitoring
User admin@<server.domain.com> - Password as configured.

ENJOY!
